package com.drahzek.bar.model;

import java.util.ArrayList;
import java.util.List;

public class Bar {
    /**
     * An array list representing a drink table where new drinks are stacked and are ready to be taken.
     */
    private List<String> drinkTable = new ArrayList<>();

    /**
     * Method responsible for putting down a new drink that is added to the drink table.
     * Each time the drink is added, every other thread is notified that such event has occured.
     * @param drink
     */
    public synchronized void put(String drink) {
        drinkTable.add(drink);
        notifyAll();
    }

    /**
     * Method responsible for taking the drinks from the table.
     * While the drink table is empty, it is forcing the thread using this method to wait until
     * a new one is served. If one is available, it is reducing the stack by one drink.
     * @return
     * @throws InterruptedException
     */
    public synchronized String take() throws InterruptedException {
        while (drinkTable.isEmpty()) {
            this.wait();
        }
        String ret = drinkTable.get(drinkTable.size()-1);
        drinkTable.remove(drinkTable.size()-1);
        return ret;
    }

}
