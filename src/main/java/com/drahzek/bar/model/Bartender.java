package com.drahzek.bar.model;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Bartender implements Runnable {
    private static final Logger log = Logger.getLogger(Bartender.class.getCanonicalName());

    private Bar drink;

    private boolean running = true;

    /**
     * constructor for the bartender
     * @param drink
     */
    public Bartender( Bar drink) {
        this.drink = drink;
    }

    /**
     * method responsible for stopping the running loops
     */
    public void stop() {
        running = false;
    }

    /**
     * Randomizer of drinks from the predefined list
     */
    public static String drinkRandomizer(){
        //more fluent randomizer for testing purposes
        int r = (int) (Math.random()*5);
        String randomDrink = new String [] {"Mohito", "Vodka", "Kamikaze", "Rum", "ChupaChups"}[r];
        return randomDrink;

        //different approach to the problem
        /*List<String> drinkMenu = Arrays.asList("Mohito", "Vodka", "Kamikaze", "Rum", "ChupaChups");
        int index = new Random().nextInt(drinkMenu.size());
        String randomDrink = drinkMenu.get(index);
        return randomDrink;*/
    }

    /**
     * override of a run method, contains everything we want to do in a running thread.
     * While it is running, each Bartender thread is putting down one drink to the stack,
     * then they wait for 3s before putting down the next one and so on. Each drink is randomized using
     * the drinkRandomizer method.
     */
    @Override
    public void run() {
        while (running) {
            try {
                String drink = drinkRandomizer();
                this.drink.put(drink);
                System.out.println("Bartender puts down " + drink);
                Thread.sleep(3000);

            } catch (InterruptedException ex) {
                log.log(Level.INFO, ex.getMessage(), ex);
            }
        }
    }
}
