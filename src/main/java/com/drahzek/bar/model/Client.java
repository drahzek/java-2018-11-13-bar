package com.drahzek.bar.model;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Client implements Runnable {
    private static final Logger log = Logger.getLogger(Bar.class.getCanonicalName());

    private String name;
    private Bar drink;

    private boolean running = true;

    /**
     * constructor for clients
     * @param name
     * @param drink
     */
    public Client(String name, Bar drink) {
        this.name = name;
        this.drink = drink;
    }

    /**
     * method responsible for stopping the running loops
     */
    public void stop() {
        running = false;
    }

    /**
     * override of a run method, contains everything we want to do in a running thread.
     * While it is running, each Client thread is taking one drink from the available stack,
     * then they proceed to drinking it for the number of seconds that is equal to the amount of
     * letters in the drink's name.
     */
    @Override
    public void run() {
        while (running) {
            try {
                String res = drink.take();
                Thread.sleep(res.length()*1000);
                System.out.println(name + " takes up " + res + " and is going to drink it for " + res.length() + " seconds");
            } catch (InterruptedException ex) {
                log.log(Level.INFO, ex.getMessage(), ex);
            }
        }
    }
}
