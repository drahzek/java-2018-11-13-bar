package com.drahzek.bar;

import com.drahzek.bar.model.Bar;
import com.drahzek.bar.model.Bartender;
import com.drahzek.bar.model.Client;

import java.util.Scanner;

/**
 * Author : Dominik Swierzynski
 * Main class of an application that is trying to emulate a weird kind of bar
 * where people kill each other for drinks the very moment the bartender puts
 * them on the table.
 */
public class BarMain {
    public static void main(String[] args) {
        System.out.println("To close the bar, enter q in the command line and press enter!");
        startBarServing();
    }

    /**
     * method responsible for running the threads
     */
    private static void startBarServing() {
        Bar drink = new Bar();
        Scanner scanner = new Scanner(System.in);
        boolean runningThreads = true;
        System.out.println("Opening the Bar!");

        Bartender bartender = new Bartender(drink);
        Client client1 = new Client("John", drink);
        Client client2 = new Client("Daniel", drink);
        Client client3 = new Client("Jackie", drink);
        Client client4 = new Client("Valentine", drink);
        Client client5 = new Client("Matsudaira", drink);

        Thread thread0 = new Thread(bartender);
        Thread thread1 = new Thread(client1);
        Thread thread2 = new Thread(client2);
        Thread thread3 = new Thread(client3);
        Thread thread4 = new Thread(client4);
        Thread thread5 = new Thread(client5);

        thread0.start();
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        /* a loop that is waiting for the "q" in the commandline so all of threads can be closed properly */
        while (runningThreads) {
            String command = scanner.next();

            if (command.equals("q")) {
                System.out.println("Closing the Bar!");
                bartender.stop();
                client1.stop();
                client2.stop();
                client3.stop();
                client4.stop();
                client5.stop();

                thread0.interrupt();
                thread1.interrupt();
                thread2.interrupt();
                thread3.interrupt();
                thread4.interrupt();
                thread5.interrupt();
                runningThreads = false;
            }
        }
    }
}
